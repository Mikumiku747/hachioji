# Makefile
# Daniel Selmes 2016
# Main Makefile, you use it to build the project

# Architecture options
# Used to provide architecture specific files
ARCH_PREFIX := i686-elf

# Directory names
# The places where various files and such are stored
KERN_ARCH_SRC := kernel/src/arch-$(ARCH_PREFIX)
KERN_ALL_SRC := kernel/src
KERN_ARCH_INCLUDE := kernel/include/arch-$(ARCH_PREFIX)
KERN_ALL_INCLUDE := kernel/include

# Toolchain definitions
# Define names and options for various tools
KERN_CC := $(ARCH_PREFIX)-gcc
KERN_AS := $(ARCH_PREFIX)-as
KERN_LD := $(ARCH_PREFIX)-gcc
# Toolchain options
KERN_C_INCLS ?= 
KERN_C_INCLS := -I$(KERN_ARCH_INCLUDE) -I$(KERN_ALL_INCLUDE)
KERN_CC_OPTS ?= -g -fno-omit-frame-pointer -Wfatal-errors
KERN_CC_OPTS := $(KERN_CC_OPTS) -std=c11 -ffreestanding -Wall -Wextra
KERN_AS_OPTS ?= -g
KERN_AS_OPTS := $(KERN_AS_OPTS) 
KERN_LD_OPTS ?= -ffreestanding -nostdlib -I$(KERN_ARCH_INCLUDE) -I$(KERN_ALL_INCLUDE)
KERN_LD_OPTS := $(KERN_LD_OPTS) 
KERN_LIBS ?= 
KERN_LIBS := -lgcc $(KERN_LIBS)

# Filesets
# Lists of files used in compilation
KERN_C_SOURCES := $(shell find $(KERN_ALL_SRC) \( -not -path "$(KERN_ALL_SRC)/arch-*/*" -o -path "$(KERN_ARCH_SRC)/*" \) -a -name "*.c")
KERN_S_SOURCES := $(shell find $(KERN_ALL_SRC) \( -not -path "$(KERN_ALL_SRC)/arch-*/*" -o -path "$(KERN_ARCH_SRC)/*" \) -a -name "*.s")
KERN_C_OBJS := $(patsubst %.c, kernel/obj/%.o, $(notdir $(KERN_C_SOURCES))) 
KERN_S_OBJS := $(patsubst %.s, kernel/obj/%.o, $(notdir $(KERN_S_SOURCES)))
KERN_OBJS := $(KERN_S_OBJS) $(KERN_C_OBJS) 
KERN_LINK_SCRIPT := $(KERN_ARCH_SRC)/linker.ld

# Recipes
# The acutal rules for building files

all: kernel/bin/$(ARCH_PREFIX)-kernel.bin hachioji.iso

sys: kernel/bin/$(ARCH_PREFIX)-kernel.bin grub/grub.cfg
	mkdir -p sys/boot/grub
	cp -v kernel/bin/$(ARCH_PREFIX)-kernel.bin sys/boot/kernel.bin
	cp -v README sys/README
	cp -v LICENSE sys/LICENSE
	cp grub/grub.cfg sys/boot/grub/

hachioji.iso: sys
	grub-mkrescue -o hachioji.iso sys -- --quiet

kernel/bin/$(ARCH_PREFIX)-kernel.bin: $(KERN_OBJS) $(KERN_LINK_SCRIPT)
	$(KERN_LD) -T $(KERN_LINK_SCRIPT) -o kernel/bin/$(ARCH_PREFIX)-kernel.bin $(KERN_LD_OPTS) $(KERN_OBJS) $(KERN_LIBS)

kernel/obj/%.o: $(KERN_ARCH_SRC)/%.c
	$(KERN_CC) $(KERN_CC_OPTS) $(KERN_C_INCLS) -c $< -o $@ -MMD
kernel/obj/%.o: $(KERN_ARCH_SRC)/%.s
	$(KERN_AS) $(KERN_AS_OPTS) $< -o $@
kernel/obj/%.o: $(KERN_ALL_SRC)/%.c
	$(KERN_CC) $(KERN_CC_OPTS) $(KERN_C_INCLS) -c $< -o $@ -MMD
kernel/obj/%.o: $(KERN_ALL_SRC)/%.s
	$(KERN_AS) $(KERN_AS_OPTS) $< -o $@

# Include the recipes to check dependencies
include $(wildcard obj/*.d)

clean:
	rm -rfv kernel/bin/*
	rm -rfv kernel/obj/*
	rm -rfv sys/*
	rm -rfv hachioji.iso
	
verbose: 
	@echo $(KERN_C_SOURCES)
	@echo $(KERN_C_OBJS)
	
.PHONY: clean verbose
