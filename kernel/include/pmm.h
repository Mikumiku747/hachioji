/*
 * pmm.h
 * Daniel Selmes 2016
 * Physical memory manager. 
*/

/* Include protection. */
#ifndef PMM_H
#define PMM_H

/* Includes. */
#include <stdint.h>

/* Initialise the physical memory management system. */
unsigned pmm_init(void *platform_data); 

/* Get a physical page from the pmm. */
uintptr_t *pmm_get_page(); 

/* Return a physical page to the pmm. */
void pmm_free_page(uintptr_t *pageaddr); 


#endif
