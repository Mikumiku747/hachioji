/*
 * kerror.h
 * Daniel Selmes 2016
 * Kernel error handling systems.
*/

/* Include Protection. */
#ifndef KERROR_H
#define KERROR_H

/* Includes. */

/* Function for crashing the kernel gracefully at early stages. */
void kerror_halt(unsigned code);

/* This defines an error value, for safety/sanity in the kernel, check this is 
always 0 after some calls. We have this so that not every single function in
the whole kernel is returning and unsigned int or something like that. */
extern unsigned KERNEL_INTERNAL_ERROR_CONDITION;
/* Shorthand for above name. */
#define KIEC KERNEL_INTERNAL_ERROR_CONDITION

#endif
