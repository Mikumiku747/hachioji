/*
 * gdt_i386.h
 * Daniel Selmes 2016
 * Global descriptor table functions and initialisation.
*/

/* Include Protection. */
#ifndef GDT_I386_H
#define GDT_I386_H

/* Includes. */
#include <stdint.h>


/* Structure for the GDT Pointer. */
struct GDT_ptr {
	uint16_t size;
	uint32_t address;
} __attribute__((__packed__));

struct GDT_entry {
	uint16_t limit1;
	uint16_t base1;
	uint8_t base2;
	uint8_t access;
	uint8_t flags;
	uint8_t base3;
} __attribute__((__packed__));


/* Function to encode a GDT entry. */
uint64_t GDT_encode_entry (uint32_t base, uint32_t limit, uint8_t access, uint8_t flags);

/* Function to initalise the GDT. */
void GDT_init();

/* Reloads the segment registers, Code = 1, Data = 2. */
void GDT_reload_segs(); 


#endif