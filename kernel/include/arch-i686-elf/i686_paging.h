/*
 * i686_paging.h
 * Daniel Selmes 2016
 * Functions and structures to control the paging mechanism used in the i686's
 * MMU, and to implement the temporary stack based manager. 
*/

/* Include Protection. */
#ifndef I686_PAGING_H
#define I686_PAGING_H

/* Includes. */
#include <stdint.h>

/* Sets up the stack based mapping system. */
void i686_paging_init_stack();

/* Calls for pusing and popping to the stacks. */
uint32_t *i686_paging_stack_pusha(uint32_t *paddr);
uint32_t *i686_paging_stack_popa();
uint32_t *i686_paging_stack_pushb(uint32_t *paddr);
uint32_t *i686_paging_stack_popb();
uint32_t *i686_paging_stack_pushc(uint32_t *paddr);
uint32_t *i686_paging_stack_popc();

/* Reloads the CR3 register. */
void i686_paging_reload_CR3();

#endif
