/*
 * platform_data.h
 * Daniel Selmes 2016
 * Platform dependent data structure, so that the main kernel can pass data to
 * architecture dependent strucutres. 
*/

/* Include Protection. */
#ifndef PLATFORM_DATA_H
#define PLATFORM_DATA_H

/* Includes. */
#include <stdint.h>

/* This structure contains information used when doing physical to virtual
address mapping. */
struct platform_mapping_data {
	uint32_t *pagedir_addr;
	uint32_t high_offset;
};

/* The main platform dependent structure. This is what is used to pass global
 * dependent data between the kernel. */
struct platform_data {
	uint32_t magic;
	void *mboot_info;
	struct platform_mapping_data mapping_data;
};

#endif
