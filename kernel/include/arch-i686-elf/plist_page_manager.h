/* 
 * plist_page_manager.h
 * Daniel Selmes 2016
 * A physical page manager which uses free pages themselves to store empty page
 * lists. Dyncamically growing and shrinking, neato. 
*/

/* Include protection. */
#ifndef PLIST_PAGE_MANAGER_H
#define PLIST_PAGE_MANAGER_H

/* Includes. */
#include <stdint.h>
#include <vgatext.h>
#include <multiboot_i386.h>
#include <platform_data.h>

/* Function to initialise the system. It takes platform data so it can find the
 * memory map. Returns the total number of pages, or 0 for an error case. */
unsigned plist_init(struct platform_data *platform_data);

/* Function to dump the memory map given to us by the bootloader. */
void plist_dump_mboot_mmap(struct platform_data *platform_data);

/* Function to dump back the chain of stacks until we get to the prime page 
 * stack. Don't use it before initialising the memory manager. */
void plist_dump_stack_chain();

/* Claims a page from the physical memory map. Returns the address of the page
 * you want claimed, or 0 on error. */
uint32_t plist_get_page();

/* Frees a page from the physical page map. Returns the address of the page you
 * freed, or 0 on error. Takes the address of a page, make sure it's a 4K 
 * aligned value, or else. */
unsigned plist_free_page(uint32_t *page_pointer);

#endif
