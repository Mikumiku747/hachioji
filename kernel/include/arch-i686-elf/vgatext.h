/*
 * vgatext.h
 * Daniel Selmes 2016
 * Provides functions to use the VGA text mode as a terminal. 
*/

/* Include Protection. */
#ifndef VGATEXT_H
#define VGATEXT_H

/* Includes. */
#include <stdint.h>

/* Conveience macros. */
#define VGA_WIDTH 80
#define VGA_HEIGHT 25
#define VGA_TEXTBUFFER 0xB8000

/* Color Definitions. */
#define VGAFCOL_BLACK 	0x0000
#define VGAFCOL_BLUE 	0x0100
#define VGAFCOL_GREEN 	0x0200
#define VGAFCOL_RED 	0x0400
#define VGAFCOL_WHITE 	0x0A00
#define VGABCOL_BLACK 	0x0000
#define VGABCOL_BLUE 	0x1000
#define VGABCOL_GREEN 	0x2000
#define VGABCOL_RED 	0x4000
#define VGABCOL_WHITE 	0xA000


/* Globals to keep track of terminal state. */
unsigned vga_cursor_x, vga_cursor_y;
uint16_t vgatext_color;
uint16_t *vgatext_buffer;

/* Initialises the VGA terminal and prepares all the values. */
void vgatext_init();

/* Print characters and strings. */
void vgatext_putc(char c);
void vgatext_puts(char *string);

/* Print 8 and 32 bit hex numbers. */
void vgatext_put_hexb(uint8_t num);
void vgatext_put_hexdw(uint32_t num);

/* Set terminal color. */
void vgatext_set_color(uint8_t color); 


#endif
