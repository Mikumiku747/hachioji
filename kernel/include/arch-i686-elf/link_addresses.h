/*
 * link_addresses.h
 * Daniel Selmes 2016
 * Importes symbols which are used to get information from the linker about
 * where certain sections start and end. 
*/

/* Include Protection. */
#ifndef LINK_ADDRESSES_H
#define LINK_ADDRESSES_H

/* Kernel boundaries. */
extern void *kernel_start_paddr;
extern void *kernel_end_paddr;


#endif
