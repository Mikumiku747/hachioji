/* 
 * i686_kernel_misc.h
 * Daniel Selmes 2016
 * Miscellaneous functions, macros, and headers. 
*/

/* Include Protection. */
#ifndef I686_KERNEL_MISC_H
#define I686_KERNEL_MISC_H

/* Some common includes. */
#include <stdint.h>

/* Conversion between physical and virtual addresses for the identity mapped 
portion of the kernel. */
#define VADDR(addr) (void *)((uint32_t)addr + (uint32_t)0xC0000000)	
#define PADDR(addr) (void *)((uint32_t)addr - (uint32_t)0xC0000000)


#endif
