/* 
 * kerror_i686.h
 * Daniel Selmes 2016
 * Kernel error handling capabilities specific to the x86 architecture. 
*/

/* Include Protection. */
#ifndef KERROR_I686
#define KERROR_I686

/* Includes. */
#include <stdint.h>

/* Error variable for internal kernel use. */
unsigned int KERNEL_INTERNAL_ERROR_CONDITION;
/* Shorthand for above name. */
#define KIEC KERNEL_INTERNAL_ERROR_CONDITION

/* Function to halt the processor with a known crash state. Pushes all registers
 * to the stack, fills them with 0xDEADBEEF and then hangs the cpu with no 
 * intended method of recovery, used to find that the kernel has crashed when 
 * debugging. Takes an error code which is put in EAX. */
void kerror_fatal_hang(uint32_t code);



#endif