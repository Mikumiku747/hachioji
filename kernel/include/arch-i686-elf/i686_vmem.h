/*
 * i686_vmem.h
 * Daniel Selmes 2016
 * Kernel virtual memory management functions, for allocating and managing 
 * kernel address-space functions. 
 */

/* Include Protection. */
#ifndef I686_VMEM_H
#define I686_VMEM_H

/* Includes. */
#include <stdint.h>

/* All this stuff is for kernel space memory manipulation. */

/* Used to hold the first block. */
uint32_t *kern_addr_first_block;
/* Used to keep track of the last slot we changed. */
uint32_t *kern_addr_recent_block;
uint32_t kern_addr_recent_slot;

/* This is the first virtual address that the kernel will take for heap. 
Currently, it leaves 32MB for the kernel core. */
#define KADDR_REGION_START 0xC2000000
#define KADDR_REGION_END 0xFFA00000
/* Initialises the kernel space page allocator. */
void i686_vmem_kern_init();

/* Dump the virutal memory usage structures. */
void i686_vmem_kern_dump_usage();

/* Takes an amount of address space from the kernel space address pool. It only
takes from address space, you have to come up with the physical memory to back
it up (or demand page it). length is the size of the area you want, it should
be 4k aligned. Can error out, check return for null. */
uint32_t *i686_vmem_kern_take(uint32_t length);

/* Returns a block of virtual address space to the pool (You need to tell it 
the about of space you're freeing. Can error out, check KEIC for safety. */
void i686_vmem_kern_return(uint32_t *vaddr, uint32_t length);

/* This stuff here is for user-space virtual memory_management. */
/* Initialises the user space address space control systems. */
void i686_user_vmem_init(); 

/* Dumps current userspace virtual memory state. */
void user_vmem_dump_state();

/* Makes a new userspace memory section. */

#endif
