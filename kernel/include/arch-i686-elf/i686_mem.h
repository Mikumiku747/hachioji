/*
 * i686_mem.h
 * Daniel Selmes 2016
 * Assembly routines for fast memory copying, scanning, zeroing, etc. 
*/

/* Include Protection. */
#ifndef I686_MEM
#define I686_MEM

/* Includes. */
#include <stdint.h>

/* Zeroing functions, of varying widths. */
void mem_zero4(uint32_t *addr, uint32_t length);
void mem_zero2(uint32_t *addr, uint32_t length);
void mem_zero1(uint32_t *addr, uint32_t length);

/* Fill functions, of varying witdths. */
void mem_fill4(uint32_t *addr, uint32_t length, uint32_t value);
void mem_fill2(uint32_t *addr, uint32_t length, uint16_t value);
void mem_fill1(uint32_t *addr, uint32_t length, uint8_t value);


#endif
