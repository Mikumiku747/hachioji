/*
 * multiboot_i386.h
 * Daniel Selmes 2016
 * Multiboot info structures and the like. 
*/

/* Include Protection. */
#ifndef MULTIBOOT_I386
#define MULTIBOOT_I386

/* Includes. */
#include <stdint.h>

/* Multiboot information structure. */
struct mboot_info_struct {
	uint32_t flags;
	
	uint32_t mem_lower;
	uint32_t mem_upper;
	
	uint32_t boot_device;
	
	uint32_t cmdline;
	
	uint32_t mods_count;
	uint32_t mods_addr;
	
	uint32_t symbols[4];
	
	uint32_t mmap_len;
	uint32_t mmap_addr;
	
	uint32_t drives_len;
	uint32_t drives_addr;
	
	uint32_t config_table;
	
	uint32_t boot_loader_name;
	
	uint32_t apm_table;
	
	uint32_t vbe_control_info;
	uint32_t vbe_mode_info;
	uint32_t vbe_mode;
	uint32_t vbe_iface_seg;
	uint32_t vbe_iface_off;
	uint32_t vbe_iface_len;
	
};

/* Multiboot memory map structure. */
struct mboot_mmap_struct {
	uint32_t len;
	uint64_t start;
	uint64_t size;
	uint32_t type;
};

/* Multiboot module info structure. */
struct mboot_modinfo_struct {
	uint32_t start;
	uint32_t end;
	char *string;
	uint32_t reserved;
};

#endif
