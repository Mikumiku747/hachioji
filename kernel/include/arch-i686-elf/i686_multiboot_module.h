/* 
 * i686_multiboot_module.h
 * Daniel Selmes 2016
 * Manages the loading and handling of multiboot modules
*/

/* Include Protection. */
#ifndef I668_MULTIBOOT_MODULE_H
#define I668_MULTIBOOT_MODULE_H

/* Includes. */
#include <multiboot_i386.h>
#include <platform_data.h>

/* Debug function for dumping all the modules loaded by the bootloader. */
void mboot_module_dump(struct platform_data *platform_data);

#endif