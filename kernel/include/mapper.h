/* 
 * mapper.h
 * Daniel Selmes 2016
 * Provides a way for the kernel to do arbitrary memory mapping. 
*/

/* Include Protection. */
#ifndef MAPPER_H
#define MAPPER_H

/* Includes. */
#include <stdint.h>
#include <platform_data.h>

/* Functions for general memory mapping manipulation. */

/* Initialises the mapping system. Takes a structure which contains platform 
specific mapping data, which depends on the architecture. */
void mapping_init(struct platform_mapping_data *mapping_data);

/* Generic page mapping and unmapping functions. */
/* Creates a mapping between a physical and linear address. For kernel mode use
only. */
uintptr_t mapping_map(uintptr_t phys_addr, uintptr_t linear_addr);
/* Removes the mapping for a given linear address. */
void mapping_unmap(uintptr_t linear_addr);

/* Functions for handling a stack based mapping system to temporarily map in 
various memory locations. This is so stuff like the physical memory manager and
some hardware drivers can work in a higher half environment. */

/* Initialises the temporary mapping system. */
void mapping_init_temp(struct platform_data *platform_data);

/* Pushes another page onto the respective mapping stack. */
uintptr_t *mapping_temp_push(uintptr_t *paddr);
uintptr_t *mapping_temp_pusha(uintptr_t *paddr);
uintptr_t *mapping_temp_pushb(uintptr_t *paddr);
uintptr_t *mapping_temp_pushc(uintptr_t *paddr);

/* Pops the mapping from the respective mapping stack. */
void mapping_temp_pop();
void mapping_temp_popa();
void mapping_temp_popb();
void mapping_temp_popc();


#endif
