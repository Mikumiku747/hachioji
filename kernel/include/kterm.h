/* 
 * kterm.h
 * Daniel Selmes 2016
 * Early life kernel output systems. Mostly platform dependent code.
*/

/* Include Protection. */
#ifndef KTERM_H
#define KTERM_H

/* Initialises the kernel output terminal. */
void kterm_init();

/* Print characters and strings to the terminal. */
void kterm_putc(char c);
void kterm_puts(char *string);

/* Print out numbers in hexidecimal. */
void kterm_puti_uh(int num);

#endif
