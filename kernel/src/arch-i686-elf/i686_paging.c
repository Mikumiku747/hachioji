/*
 * i686_paging.c
 * Daniel Selmes 2016
 * Implements the paging systems on the x86 platform. 
*/

#include <i686_paging.h>
#include <i686_kernel_misc.h>

/* The temporary paging system uses three stacks to allow you to temporarily 
map in any physical page to a fixed address. When you push a page, it is pushed
onto the stack and mapped in, and when you pop a page, it is popped out and the
previous mapping is restored. That way, things can use it without having to care
about what is currently being temporarily accessed. */

/* This structure holds the mapping infomation stacks. */
__attribute__((aligned(4096))) struct {
	uint32_t *mapping_stack_a[512];
	uint32_t *mapping_stack_b[256];
	uint32_t *mapping_stack_c[256];
} stack_mappings;
/* We use this as a page table to set up the mappings. */
uint32_t stacks_pagetab[1024] __attribute__((aligned(4096)));
/* These counters are used to keep the index of the currently mapped page in 
the stack. */
unsigned stack_index_a;
unsigned stack_index_b;
unsigned stack_index_c;

/* Initialises the temporary memory mapping stack system. */
void i686_paging_init_stack()
{
	/* First thing to do is put the stacks all to the default state, and setup 
	the sentinel values in the stacks. */
	stack_index_a = 0;
	stack_mappings.mapping_stack_a[0] = (uint32_t *)0xFFFFFFFF;
	stack_index_b = 0;
	stack_mappings.mapping_stack_a[0] = (uint32_t *)0xFFFFFFFF;
	stack_index_c = 0;
	stack_mappings.mapping_stack_a[0] = (uint32_t *)0xFFFFFFFF;
	/* Now we have to set up the actual page mappings. This means configuring  
	and adding the page table that contains mappings for the 3 stack pages. By
	default we just map them to 0. */
	stacks_pagetab[0] = ((uint32_t)(0) & 0xFFFFF000) | 0x03;
	stacks_pagetab[1] = ((uint32_t)(0) & 0xFFFFF000) | 0x03;
	stacks_pagetab[2] = ((uint32_t)(0) & 0xFFFFF000) | 0x03;
	/* Now, lastly, we actually map it in using the recursive access point at 
	the end of the page directory. */
	((uint32_t *)0xFFFFF000)[1022] = (uint32_t)PADDR(&stacks_pagetab) | 0x03;
	i686_paging_reload_CR3();
}

/* Pushes a value onto the stack. Returns address to use on success and 0 on 
failure. paddr is the address of the physical page you want to map in. */
uint32_t *i686_paging_stack_pusha(uint32_t *paddr)
{
	if (stack_index_a >= 511) {
		/* Stack is full, return a 0. */
		return 0;
	}
	else {
		/* Add the address to the stack. */
		stack_index_a++;
		stack_mappings.mapping_stack_a[stack_index_a] = paddr;
		/* Change the page mappings to make it reflect the new top value. */
		((uint32_t *)(0xFFFFE000))[0] = ((uint32_t)(((uint32_t)paddr & 0xFFFFF000) | 0x03));
		i686_paging_reload_CR3();
		return (uint32_t *)0xFF800000;
	}
}

/* Pops a value from the stack. Returns the new physical address that this
stack is now mapped to, or 0 for failure. */
uint32_t *i686_paging_stack_popa()
{
	if (stack_index_a == 0) {
		/* Stack is empty, can't pop. Return a 0. */
		return 0;
	}
	else {
		/* Pop the last address off the stack. */
		stack_index_a--;
		/* Change the mappings to reflect the old value. */
		((uint32_t *)(0xFFFFE000))[0] = (uint32_t)(stack_mappings.mapping_stack_a[stack_index_a]);
		i686_paging_reload_CR3();
		return (uint32_t *)(((uint32_t *)(0xFFFFE000))[0]);
	}
}

/* The stack push and pop methods are repeated for the B and C stacks. */
/* Pushes a value onto the stack. Returns address to use on success and 0 on
failure. paddr is the address of the physical page you want to map in. */
uint32_t *i686_paging_stack_pushb(uint32_t *paddr)
{
	if (stack_index_b >= 255) {
		/* Stack is full, return a 0. */
		return 0;
	}
	else {
		/* Add the address to the stack. */
		stack_index_b++;
		stack_mappings.mapping_stack_b[stack_index_b] = paddr;
		/* Change the page mappings to make it reflect the new top value. */
		((uint32_t *)(0xFFFFE000))[1] = ((uint32_t)(((uint32_t)paddr & 0xFFFFF000) | 0x03));
		i686_paging_reload_CR3();
		return (uint32_t *)0xFF801000;
	}
}

/* Pops a value from the stack. Returns the new physical address that this
stack is now mapped to, or 0 for failure. */
uint32_t *i686_paging_stack_popb()
{
	if (stack_index_b == 0) {
		/* Stack is empty, can't pop. Return a 0. */
		return 0;
	}
	else {
		/* Pop the last address off the stack. */
		stack_index_b--;
		/* Change the mappings to reflect the old value. */
		((uint32_t *)(0xFFFFE000))[1] = (uint32_t)(stack_mappings.mapping_stack_b[stack_index_b]);
		i686_paging_reload_CR3();
		return (uint32_t *)(((uint32_t *)(0xFFFFE000))[1]);
	}
}

/* Pushes a value onto the stack. Returns address to use on success and 0 on
failure. paddr is the address of the physical page you want to map in. */
uint32_t *i686_paging_stack_pushc(uint32_t *paddr)
{
	if (stack_index_c >= 255) {
		/* Stack is full, return a 0. */
		return 0;
	}
	else {
		/* Add the address to the stack. */
		stack_index_c++;
		stack_mappings.mapping_stack_c[stack_index_c] = paddr;
		/* Change the page mappings to make it reflect the new top value. */
		((uint32_t *)(0xFFFFE000))[2] = ((uint32_t)(((uint32_t)paddr & 0xFFFFF000) | 0x03));
		i686_paging_reload_CR3();
		return (uint32_t *)0xFF802000;
	}
}

/* Pops a value from the stack. Returns the new physical address that this
stack is now mapped to, or 0 for failure. */
uint32_t *i686_paging_stack_popc()
{
	if (stack_index_c == 0) {
		/* Stack is empty, can't pop. Return a 0. */
		return 0;
	}
	else {
		/* Pop the last address off the stack. */
		stack_index_c--;
		/* Change the mappings to reflect the old value. */
		((uint32_t *)(0xFFFFE000))[2] = (uint32_t)(stack_mappings.mapping_stack_c[stack_index_c]);
		i686_paging_reload_CR3();
		return (uint32_t *)(((uint32_t *)(0xFFFFE000))[2]);
	}
}

/* Reloads the CR3 register, which invalidates any page mappings caches that
are still in effect. */
void i686_paging_reload_CR3() {
	__asm__("\n\t\
		movl %%cr3, %%eax \n\t\
		movl %%eax, %%cr3"
		: :
		: "eax");
}

/* Maps a given physical address to a given virtual address. Flags is the lower
12 bits used for page options. Returns the virtual address you passed in on
success, or 0 on failure. */
uint32_t *i686_paging_map_page(uint32_t *paddr, uint32_t *vaddr, uint16_t flags)
{
	/* First, we check whether we're doing a 4KiB or 4MiB page map. */
	if ((flags & 0x0080) != 0) {
		/* 4MB page map. Check the addresses are valid. */
		if ((((uint32_t)paddr & 0xFFC00000) != 0) || (((uint32_t)vaddr & 0xFFC00000) != 0)) {
			return 0;
		}
		((uint32_t *)(0xFFFFF000))[(uint32_t)vaddr >> 22] = (uint32_t)vaddr | (flags & 0x0FFF);
		return vaddr;
	} 
	else {
		/* 4KiB page mapping. Check addresses are valid. */
		if ((((uint32_t)paddr & 0xFFFFF000) != 0) || (((uint32_t)vaddr & 0xFFFFF000) != 0)) {
			return 0;
		}
		/* Check that the required page table is present. */
		if (((((uint32_t *)(0xFFFFF000))[(uint32_t)vaddr >> 22 & 0x000003FF]) & 0x00000001) == 0) {
			/* The page table is not present, so we can't set that one. */
			return 0;
		}
		*((uint32_t *)(0xFFC00000 + (((uint32_t)vaddr >> 10) & 0x003FFFFF))) = ((uint32_t)paddr & 0xFFFFF000) | ((uint32_t)flags & 0x00000FFF);
		return vaddr;
	}
	
}

/* Unmap a given virtual address. vaddr is the virtual address to be unmapped. 
Returns the address that you just unmapped, or 0 on failure. */
uint32_t *i686_paging_unmap_page(uint32_t *vaddr)
{
	/* Check the address is valid. */
	if (((uint32_t)vaddr & 0xFFFFF000) != 0) {
		return 0;
	}
	/* Check if the containing page table is present. */
	if ((((uint32_t *)0xFFC00000)[((uint32_t)vaddr >> 22) & 0x3FF] & 0x00000001) == 0) {
		/* Table not present, so it can't be un-mapped. */
		return vaddr;
	}
	/* Check if the page itself is present. */
	if ((*((uint32_t *)(0xFFC00000 + (((uint32_t)vaddr >> 10) & 0x003FFFFF))) & 0x00000001) == 0) {
		/* Page is already non-present, nothing to be done. */
		return vaddr;
	}
	else {
		/* It's present, let's make it not so. */
		*((uint32_t *)(0xFFC00000 + (((uint32_t)vaddr >> 10) & 0x003FFFFF))) = 0;
		return vaddr;
	}
}

