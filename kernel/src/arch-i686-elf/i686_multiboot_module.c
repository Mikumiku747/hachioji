/*
 * i686_multiboot_module.c
 * Daniel Selmes 2016
 * Manages the loading and handling of multiboot modules
*/

/* Platform Specific Includes. */
#include <i686_multiboot_module.h>
#include <vgatext.h>
#include <multiboot_i386.h>
#include <platform_data.h>
#include <i686_kernel_misc.h>
/* Platform Agnostic Includes. */
#include <mapper.h>

/* Dumps information about the modules loaded by the bootloader. */
void mboot_module_dump(struct platform_data *platform_data) 
{
	/* First, get the multiboot structure out of this. */
	struct mboot_info_struct *mbinfo = platform_data->mboot_info;
	/* Get the module table and fix address. */
	struct mboot_modinfo_struct *modinfo = (struct mboot_modinfo_struct *)(VADDR(mbinfo->mods_addr));
	/* Display basic information. */
	vgatext_puts("Number of Modules Loaded: 0x");
	vgatext_put_hexdw(mbinfo->mods_count);
	vgatext_puts(", module table at 0x");
	vgatext_put_hexdw(mbinfo->mods_addr);
	vgatext_puts(".\n");
	
	for (int modindex = 0; modindex < mbinfo->mods_count; modindex++) {
		/* Dump the module's basic info. */
		vgatext_puts("    0x");
		vgatext_put_hexdw(modinfo[modindex].start);
		vgatext_puts(" - 0x");
		vgatext_put_hexdw(modinfo[modindex].end);
		vgatext_puts(": ");
		vgatext_puts((char *)(VADDR(modinfo[modindex].string)));
		vgatext_puts("\n    ");
		/* Provide a preview of the first 30 bytes and first 62 characters. */
		vgatext_puts("0x");
		char *snapshot = mapping_temp_push(modinfo[modindex].start);
		for (int b = 0; b < 30; b++) {
			vgatext_put_hexb(snapshot[b]);
			
		}
		vgatext_puts("\n    ");
		for (int c = 0; c < 62; c++) {
			if (snapshot[c] == '\n') {c = 62; break;}
			vgatext_putc(snapshot[c]);
		}
		vgatext_putc('\n');
		mapping_temp_pop();
	}
	
}
