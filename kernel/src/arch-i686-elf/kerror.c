/*
 * kerror.c
 * Daniel Selmes 2016
 * Kernel error handling systems. 
*/

#include <kerror.h>
#include <kerror_i686.h>
#include <stdint.h>

void kerror_halt(unsigned code) 
{
	kerror_fatal_hang((uint32_t)code);
}

