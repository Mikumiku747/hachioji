# kerror_i686.s
# Daniel Selmes 2016
# Error handling routines for low level controlled crashes. 

.global kerror_fatal_hang

.section .text
.type kerror_fatal_hang STT_FUNC
kerror_fatal_hang:
	pushl 	%eax
	movl 	-8(%esp), %eax
	pushal
	movl 	 $0xDEADBEEF, %ebx
	movl 	 $0xDEADBEEF, %ecx
	movl 	 $0xDEADBEEF, %edx
	movl 	 $0xDEADBEEF, %esi
	movl 	 $0xDEADBEEF, %edi
	
