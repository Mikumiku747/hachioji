/* 
 * mapper.c
 * Daniel Selmes 2016
 * Functions and data to control mapping of linear to physical memory. 
*/

/* Includes. */
#include <mapper.h>
#include <i686_paging.h>

void mapping_init_temp(struct platform_data *platform_data) {
	/* We'll set up the stacks and do a quick test with them. */
	i686_paging_init_stack();
}

/* Performs a push to the respective stacks. */
uintptr_t *mapping_temp_push(uintptr_t *paddr)
{
	return (uintptr_t *)i686_paging_stack_pusha((uint32_t *)paddr);
}
uintptr_t *mapping_temp_pusha(uintptr_t *paddr)
{
	return (uintptr_t *)i686_paging_stack_pusha((uint32_t *)paddr);
}
uintptr_t *mapping_temp_pushb(uintptr_t *paddr)
{
	return (uintptr_t *)i686_paging_stack_pushb((uint32_t *)paddr);
}
uintptr_t *mapping_temp_pushc(uintptr_t *paddr)
{
	return (uintptr_t *)i686_paging_stack_pushc((uint32_t *)paddr);
}
/* Performs a pop on the respective stacks. */
void mapping_temp_pop() 
{
	i686_paging_stack_popa();
}
void mapping_temp_popa()
{
	i686_paging_stack_popa();
}
void mapping_temp_pobp()
{
	i686_paging_stack_popb();
}
void mapping_temp_popc()
{
	i686_paging_stack_popc();
}

