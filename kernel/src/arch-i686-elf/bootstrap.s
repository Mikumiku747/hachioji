# bootstrap.s
# Daniel Selmes 2016
# Bootstrap to get the C part of kernel init running on x86

# Define the offset for the higher half
.set HIGHER_OFFSET, 0xC0000000

# Multiboot header definition
# First we set the values we'd like, and then we declare the header itself
.set ALIGN, 	1 << 0 		# Request module alignment
.set MEMINFO, 	1 << 1 		# Request memory info
.set FLAGS, 	ALIGN | MEMINFO		# Header flags
.set MAGIC, 	0x1BADB002	# Multiboot magic number
.set CHECKSUM, 	-(FLAGS + MAGIC)	# Multiboot header checksum
# Now we actually define the header
.section .multiboot, "a", @progbits
.align 4
.long MAGIC
.long FLAGS
.long CHECKSUM
.rept 5 	# Empty space for program headers
	.long 0
.endr
.rept 3 	# Empty space for graphics header
	.long 0
.endr

# Set up the stack, it's a temporary 16KiB one till we get the real one working
.section .bss
.align 16
stack_bottom:
.skip 16384
stack_top:
# Set up some space for bootstrap page tables. 
.global bs_pagedir
.align 4096
bs_pagedir: 
.skip 4096

# The bootstrap routine to get a C friendly environment running. We get paging
# going and then call into the C part of the kernel. 
.section .text
.global bootstrap_entry
.type bootstrap_entry STT_FUNC
bootstrap_entry:
	# Set up the stack
	movl $(stack_top  - HIGHER_OFFSET), %esp
	# Preserve multiboot info (and get ready for C call)
	push %ebx
	push %eax
	# Now we set up the paging tables. 
	movl $(bs_pagedir - HIGHER_OFFSET), %edi		# Load address of the paging structure
	# Identity map first 16MB 
	movl $0x00000083, 4*000(%edi)
	movl $0x00400083, 4*001(%edi)
	movl $0x00800083, 4*002(%edi)
	movl $0x00C00083, 4*003(%edi)
	# High map first 16MB 
	movl $0x00000083, 4*768(%edi) 
	movl $0x00400083, 4*769(%edi) 
	movl $0x00800083, 4*770(%edi) 
	movl $0x00C00083, 4*771(%edi)
	# Set up the recursive mapping in the final entry. 
	movl %edi, %eax
	orl $0x03, %eax
	movl %eax, 4*1023(%edi)
	# Load the page table
	movl $(bs_pagedir - HIGHER_OFFSET), %eax
	movl %eax, %cr3
	# Enable PSE
	movl %cr4, %eax
	orl $0x00000010, %eax
	movl %eax, %cr4
	# Enable Paging
	movl %cr0, %eax
	orl $0x80010000, %eax
	movl %eax, %cr0
	# Move info higher half code
	mov $high_code, %eax
	jmp *%eax
	high_code:
	# Fix the stack
	addl $0xC0000000, %esp
	# Remove the identity mappings. 
	movl $bs_pagedir, %edi		# Load address of the paging structure
	# Clear identity maping of the first 16MB 
	movl $0x00000000, 4*000(%edi)
	movl $0x00000000, 4*001(%edi)
	movl $0x00000000, 4*002(%edi)
	movl $0x00000000, 4*003(%edi) 
	# Reload CR3 so ident paging is no longer in effect
	movl %cr3, %eax
	movl %eax, %cr3
	# Call into the C part of the kernel
	mov $x86_bootstrap, %eax
	call *%eax
.hang:
	cli
	hlt
	jmp .hang
