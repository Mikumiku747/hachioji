/*
 * x86_bootstrap.c
 * Daniel Selmes 2016
 * Code to help get the kernel ready to go into the kernel proper. 
*/

/* Includes. */
#include <stdint.h>
#include <platform_data.h>
#include <gdt_i386.h>
#include <i686_kernel_misc.h>

/* Bring in some symbols from the bootstrap assembly. */
extern uint32_t *bs_pagedir;

/* Pre-declare the kernel's main routine so we can use it properly. */
extern void kernel_main(void *pdata_ptr);

void x86_bootstrap(uint32_t magic, void *mboot)
{
	struct platform_data x86_data = {0};
	/* Let's add the multiboot data to the platform independent data 
	 * store.*/
	x86_data.magic = magic;
	x86_data.mboot_info = (void *)VADDR(mboot);
	x86_data.mapping_data.pagedir_addr = (uint32_t *)&bs_pagedir;
	x86_data.mapping_data.high_offset = (uint32_t)VADDR(0);
	/* We'll initialise the GDT. */
	GDT_init();
	/* We're done with preparation for the main kernel, so let's go into it
	 * and hang if we ever get back. */
	kernel_main((struct platform_data *)&x86_data);
	while (1);
	
}