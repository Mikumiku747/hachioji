/*
 * plist_page_manager.c
 * Daniel Selmes 2016
 * Page manager which uses free pages themselves to hold lists of free pages.
*/

/* Includes. */
#include <plist_page_manager.h>
#include <link_addresses.h>
#include <i686_paging.h>
#include <i686_kernel_misc.h>

/* This array is what the system initially uses to store free pages. When this
 * thing runs out, it's game over, and we have to start finding ways to free up
 * physical pages. It has a sentinel value at the bottom of it. We force it to
 * be page aligned for consistency. */
uint32_t prime_free_page_store[1024] __attribute__((aligned(4096)));

/* This master pointer keeps track of which page we are currently using as the
 * free page stack. */
uint32_t *free_page_stack;
/* This counter keeps track of the next open slot in the page stack. We
 * increment every time we get a new free page, and decrement every time we
 * hand one out. */
unsigned free_page_stack_counter;

/* plist_init
 * Function to initialise the system. It takes platform data so it can find the
 * memory map. Returns the total number of pages, or 0 for an error case. This
 * function makes use of definitions provided by the linker, which are included
 * from link_addresses.h.
*/
unsigned plist_init(struct platform_data *platform_data)
{
	/* This keeps track of how much memory we've initialised. */
	unsigned free_pages = 0;
	/* This is for when we need to make physical accesses outside of the kernel
	 * memory area. */
	uint32_t *temp_map;
	/* First, we initialise the prime page store. */
	prime_free_page_store[0] = 0xFFFFFFFF;
	for (int i = 1; i < 1024; i++) {
		prime_free_page_store[i] = 0x00;
	}
	free_page_stack = PADDR(prime_free_page_store);
	free_page_stack_counter = 1;
	/* Now, we'll iterate through each region given to us by GRUB and add
	 * all the pages it contains to the free page things. */
	struct mboot_info_struct *mbinfo = (struct mboot_info_struct *)(platform_data->mboot_info);
	uint32_t left = mbinfo->mmap_len;
	struct mboot_mmap_struct *entry = VADDR((struct mboot_mmap_struct *)(mbinfo->mmap_addr));
	while (left > 0) {
		/* We want to do this pagewise, so we round the start address up
		 * to the next page, and the end address down to the next lowest
		 * page border minus 1. We'll ignore any entries above the 4G
		 * mark. Also, we need to make sure it's actually usable too. */
		if (entry->type == 1 && entry->start < 0xFFFFFFFF && entry->start + entry->size < 0xFFFFFFFF) {
			uint32_t start = (uint32_t)(entry->start);
			if ((start & 0x00000FFF) != 0) {
				start = ((entry->start) | 0x00000FFF) + 1;
			} 
			uint32_t end = ((uint32_t)(entry->start) + (uint32_t)(entry->size)) - 1;
			if ((end & 0x00000FFF) != 0xFFF) {
				end = (end & 0xFFFFF000) - 1;
			}
			/* Now, we can iterate through all of the pages in a given 
			* section. */
			for (uint32_t page_addr = start; page_addr < end; page_addr += 0x1000) {
				/* We need to check every page to make sure nothing of
				* any importance is there, like the kernel or hardware
				* or something. */
				/* Nothing below 1MB */
				if (page_addr <= 0x00100000) {continue;}
				/* Nothing inside the kernel. */
				if (page_addr > (uint32_t)PADDR(&kernel_start_paddr) && page_addr < (uint32_t)PADDR(&kernel_end_paddr)) {continue;}
				/* Check the page doesn't contain a kernel module: */
				struct mboot_modinfo_struct *modinfo = VADDR((struct mboot_modinfo_struct*)(mbinfo->mods_addr));
				int istakenbymod = 0;
				for (int modindex = 0; modindex < mbinfo->mods_count; modindex++) {
					if (page_addr >= ((modinfo[modindex]).start & 0xFFFFF000) &&
						page_addr <= ((modinfo[modindex]).end & 0xFFFFF000)) {
						/* It's on a bad page, so we need to skip through! */
						istakenbymod = 1;
					}
				}
				/* Skip it if it was taken by a module. We can't use continue from inside the for loop. */
				if (istakenbymod) { continue; }
				/* OK, all good, add it onto the list! */
				if (free_page_stack_counter > 1023) {
					temp_map = i686_paging_stack_pusha((uint32_t *)page_addr);
					temp_map[0] = (uint32_t)free_page_stack;
					i686_paging_stack_popa();
					free_page_stack = (uint32_t *)page_addr;
					free_page_stack_counter = 1;
					free_pages++;
				} else {
					temp_map = i686_paging_stack_pusha(free_page_stack);
					temp_map[free_page_stack_counter] = page_addr;
					i686_paging_stack_popa();
					free_page_stack_counter++;
					free_pages++; 
				}
			}
		}
		/* Move on to the next entry. */
		left -= ((entry->len) + 4);
		entry = (struct mboot_mmap_struct *)((uint32_t)entry + (uint32_t)((entry->len) + 4));
	}
	return free_pages;
}

/* Function to dump the memory map given to us by the bootloader. */
void plist_dump_mboot_mmap(struct platform_data *platform_data)
{
	/* First, let's get an actual reference to the memory map list. */
	struct mboot_info_struct *mbinfo = (struct mboot_info_struct *)(platform_data->mboot_info);
	vgatext_puts("\nMemory Map Address: 0x");
	vgatext_put_hexdw(mbinfo->mmap_addr);
	vgatext_puts("\nMemory Map Length: 0x");
	vgatext_put_hexdw(mbinfo->mmap_len);
	/* Now we'll iterate through it and print out all the entries. */
	uint32_t left = mbinfo->mmap_len;
	struct mboot_mmap_struct *entry = VADDR((struct mboot_mmap_struct *)(mbinfo->mmap_addr));
	while(left > 0) {
		vgatext_puts("\nStart:0x");
		vgatext_put_hexdw((uint32_t)(entry->start));
		vgatext_puts(" End:0x");
		vgatext_put_hexdw((uint32_t)(entry->start + entry->size - 1));
		vgatext_puts(" Size:0x");
		vgatext_put_hexdw((uint32_t)(entry->size));
		vgatext_puts(" Type:0x");
		vgatext_put_hexdw((uint32_t)(entry->type));
		left -= ((entry->len) + 4);
		entry = (struct mboot_mmap_struct *)((uint32_t)entry + (uint32_t)((entry->len) + 4));
	}
	/* And for good measure, dump the kernel area too. */
	vgatext_puts("\nKernel Start: 0x");
	vgatext_put_hexdw((uint32_t)PADDR(&kernel_start_paddr));
	vgatext_puts("\nKernel End: 0x");
	vgatext_put_hexdw((uint32_t)PADDR(&kernel_end_paddr));
	vgatext_puts("\n");
}

/* Function to dump back the chain of stacks until we get to the prime page 
 * stack. Don't use it before initialising the memory manager. */
void plist_dump_stack_chain()
{
	/* Just go through these until we hit the sentinel value. */
	uint32_t *current_stack = free_page_stack;
	uint32_t *temp_map = i686_paging_stack_pusha(current_stack);
	vgatext_puts("Memory Stack Chain Dump - Top: 0x");
	vgatext_put_hexdw((uint32_t)current_stack);
	while (temp_map[0] != 0xFFFFFFFF) {
		current_stack = (uint32_t *)(temp_map[0]);
		i686_paging_stack_popa();
		temp_map = i686_paging_stack_pusha(current_stack);
		vgatext_puts(", 0x");
		vgatext_put_hexdw((uint32_t)current_stack);
	}
	i686_paging_stack_popa();
	vgatext_puts(", END\n");
	
}

/* Claims a page from the physical memory map. Returns the address of the page
 * you want claimed, or 0 on error. */
uint32_t plist_get_page() 
{
	/* Make a pointer for doing physical accesses. */
	uint32_t *temp_map;
	/* It's very simple, just return the page at the top of the stack. */
	if (free_page_stack_counter == 1) {
		/* Check we actually have memory left. */
		temp_map = i686_paging_stack_pusha(free_page_stack);
		if (temp_map[0] == 0xFFFFFFFF) { return 0;}
		i686_paging_stack_popa();
		uint32_t temp = (uint32_t)free_page_stack;
		temp_map = i686_paging_stack_pusha(free_page_stack);
		free_page_stack = (uint32_t *)temp_map[0];
		i686_paging_stack_popa();
		free_page_stack_counter = 1024;
		return temp;
	} else {
		/* Normally, we just pop a page off the stack. */
		temp_map = i686_paging_stack_pusha(free_page_stack);
		return temp_map[--free_page_stack_counter];
		i686_paging_stack_popa();
	}
	return 0;
}

/* Frees a page from the physical page map. Returns the address of the page you
 * freed, or 0 on error. Takes the address of a page, make sure it's a 4K 
 * aligned value, or else. */
unsigned plist_free_page(uint32_t *page_pointer)
{
	/* Used to make physical accesses. */
	uint32_t *temp_map;
	/* It's very simple, we just add the page to the top of the stack. */
	if ((uint32_t)page_pointer && 0x00000FFF != 0x00000000) {return 0;}
	if (free_page_stack_counter == 1024) {
		temp_map = i686_paging_stack_pusha(page_pointer);
		((uint32_t *)temp_map)[0] = (uint32_t)free_page_stack;
		i686_paging_stack_popa();
		free_page_stack = page_pointer;
		free_page_stack_counter = 1;
		return (unsigned)page_pointer;
	} else {
		temp_map = i686_paging_stack_pusha(free_page_stack);
		temp_map[++free_page_stack_counter] = (uint32_t)page_pointer;
		i686_paging_stack_popa();
		return (unsigned)page_pointer;
	}
	return 0;
}
