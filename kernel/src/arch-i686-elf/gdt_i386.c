/*
 * gdt_i386.c
 * Daniel Selmes 2016
 * Global Descriptor Table functions.
*/

/* Includes. */
#include <gdt_i386.h>

#define GDT_LENGTH 3

/* The actual GDT itself. */
uint64_t kernel_GDT[GDT_LENGTH];
/* GDT pointer. */
struct GDT_ptr kernel_GDT_ptr;

/* Function to encode a GDT entry. */
uint64_t GDT_encode_entry (uint32_t base, uint32_t limit, uint8_t access, uint8_t flags)
{
	uint64_t entry = 0;
	struct GDT_entry *entryS = (struct GDT_entry *)&entry;
	entryS->base1 = (uint16_t)(base & 0x0000FFFF);
	entryS->base2 = (uint8_t)((base & 0x00FF0000)>>16);
	entryS->base3 = (uint8_t)((base & 0xFF000000)>>24);
	entryS->limit1 = (uint16_t)(limit & 0x0000FFFF);
	entryS->flags = (uint8_t)((flags & 0xF0)|((limit & 0x000F0000)>>16));
	entryS->access = access;
	return entry;
}

/* Function to initalise the GDT. */
void GDT_init() {
	/* Null entry. */
	kernel_GDT[0] = GDT_encode_entry(0, 0, 0, 0);
	/* Code segment. */
	kernel_GDT[1] = GDT_encode_entry(0x00000000, 0x00FFFFFF, 0b10011010, 0b11000000);
	/* Data segment. */
	kernel_GDT[2] = GDT_encode_entry(0x00000000, 0x00FFFFFF, 0b10010010, 0b11000000);
	/* Fill in the GDT. */
	kernel_GDT_ptr.address = (uint32_t)&kernel_GDT;
	kernel_GDT_ptr.size = (8 * GDT_LENGTH - 1);
	/* Load the GDT. */
	__asm__("lgdt (%0);"
		:
		: "r" (&kernel_GDT_ptr)
		:
	);
	GDT_reload_segs();
}

/* Reloads the segment registers, Code = 1, Data = 2. */
void GDT_reload_segs() 
{
	/* Reload segment registers. */
	__asm__("ljmp %0, $reload_CS\n\
		 reload_CS:\n\t\
		 movl %1, %%ds\n\t\
		 movl %1, %%es\n\t\
		 movl %1, %%fs\n\t\
		 movl %1, %%gs\n\t\
		 movl %1, %%ss\n\t"
		: 
		: "g" (sizeof(struct GDT_entry)*1), "r" (sizeof(struct GDT_entry)*2)
		:
	);
}
