/* 
 * kterm.h
 * Daniel Selmes 2016
 * Provides early life kernel output. This pretty much acts as a wrapper to 
*/

#include <kterm.h>
#include <stdint.h>
#include <vgatext.h>

/* Initialises the kernel output terminal. */
void kterm_init()
{
	vgatext_init();
}

/* Print characters and strings to the terminal. */
void kterm_putc(char c)
{
	vgatext_putc(c);
}
void kterm_puts(char *string)
{
	vgatext_puts(string);
}
/* Print out numbers in hexidecimal. */
void kterm_puti_uh(int num)
{
	vgatext_put_hexdw((uint32_t)num);
}
