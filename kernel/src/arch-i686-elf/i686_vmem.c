/*
 * i686_vmem.c
 * Daniel Selmes 2016
 * Virtual memory manipulation functions to map kernel heap pages in and out of
 * memory. 
*/

/* Includes. */
#include <i686_vmem.h>
#include <plist_page_manager.h>
#include <i686_paging.h>
#include <i686_mem.h>
#include <kerror_i686.h>
#include <stddef.h>

void i686_vmem_kern_init()
{
	/* This initialises the kernel page heap. We just want to set up the first block for now. */
	/* Get and map a page for the block. */
	kern_addr_first_block = (uint32_t *)plist_get_page();
	uint32_t *first_block_mapped = i686_paging_stack_pusha(kern_addr_first_block);
	/* Zero out the first block. */
	mem_zero4(first_block_mapped, 4096/4);
	/* Put the kernel's heap region in the first slot. */
	first_block_mapped[2] = KADDR_REGION_START;
	first_block_mapped[3] = (KADDR_REGION_END - KADDR_REGION_START);
	/* Prepare the heuristics information. */
	kern_addr_recent_block = kern_addr_first_block;
	kern_addr_recent_slot = 1;
	/* Unmap the first bitmap. */
	i686_paging_stack_popa();
	
}

uint32_t *i686_vmem_kern_take(uint32_t length)
{
	/* Verify the length is really aligned. */
	if ((length & 0x00000FFF) != 0) {
		return NULL;
	}
	/* Search the last slot to see if it has enough space. */
	uint32_t *recent_block_mapped = i686_paging_stack_pusha(kern_addr_recent_block);
	if (recent_block_mapped[kern_addr_recent_slot * 2 + 1] != 0 && 
		recent_block_mapped[kern_addr_recent_slot * 2 + 1] <= length) {
		/* There's enough space in the most recent slot. */
		recent_block_mapped[kern_addr_recent_slot * 2 + 1] -= length;
		return (uint32_t *)(recent_block_mapped[kern_addr_recent_slot * 2] 
							+ recent_block_mapped[kern_addr_recent_slot * 2 + 1]);
	}
	/* Search through all the slots in this map. */
	for (int slot = 1; slot < 1024; slot++) {
		if (recent_block_mapped[slot * 2 + 1] != 0 &&
			recent_block_mapped[slot * 2 + 1] != 0) {
			/* There is enough space in this slot. */
			recent_block_mapped[slot * 2 + 1] -= length; 
			kern_addr_recent_slot = slot;
			return (uint32_t *)(recent_block_mapped[slot * 2] + recent_block_mapped[slot * 2 + 1]);
		}
	}
	/* Coulndn't find a slot in the recent block. */
	i686_paging_stack_popa();

	/* Search through all slots in all blocks. */
	uint32_t *block = kern_addr_first_block;
	uint32_t *block_mapped;
	do {
		block_mapped = i686_paging_stack_pusha(block); 
		if (block != kern_addr_recent_block) { /* Skip the block if it's the recent one (we already searched it). */
			for (int slot = 1; slot < 1024; slot++) {
				if (block_mapped[slot * 2 + 1] != 0 && block_mapped[slot * 2 + 1] >= length) {
					/* This slot has space. */
					block_mapped[slot * 2 + 1] -= length;
					kern_addr_recent_slot = slot;
					kern_addr_recent_block = block;
					/* Store the value we want to return. */
					uint32_t *temp = (uint32_t *)(block_mapped[slot * 2] + block_mapped[slot * 2 + 1]);
					i686_paging_stack_popa();
					return temp;
				}
			}
		}
		/* No slots found in this block, prep the next one. */
		block = (uint32_t *)(block_mapped[1]);
		i686_paging_stack_popa();
	} while (block != 0); /* Break when we hit the last block. */
	i686_paging_stack_popa();
	return 0;
}

void i686_vmem_kern_return(uint32_t *vaddr, uint32_t length) {
	/* Make sure the paramaters passed are valid. */
	if ((((uint32_t)(vaddr) & 0x00000FFF) != 0) || (((uint32_t)(length) & 0x00000FFF) != 0)) {
		KIEC = 1;
		return;
	}
	/* First, we check if we can place in the most recent accessed block, and the one after that. */
	uint32_t *block = kern_addr_recent_block;
	uint32_t *block_mapped = i686_paging_stack_pusha(block);
	if (block_mapped[kern_addr_recent_slot * 2 + 1] == 0) {
		/* The most recent one is empty, so we can use that one. */
		block_mapped[kern_addr_recent_slot * 2 + 0] = (uint32_t)vaddr; 
		block_mapped[kern_addr_recent_slot * 2 + 1] = length;
		/* Free the mapping we used. */
		i686_paging_stack_popa();
		return; 
	}
	/* We need to make sure the slot after isn't in the next block when we check the one after. */
	if (kern_addr_recent_slot + 1 >= 1024) {
		/* Let's try mapping the next block and doing the first slot from that. */
		if (block_mapped[1] != 0) {
			/* There is another block, let's map it in and check the first slot. */
			uint32_t *next_mapped = i686_paging_stack_pusha((uint32_t *)(block_mapped[1]));
			if (next_mapped[1 * 2 + 1] == 0) {
				/* It's free, let's dump our entry there. */
				next_mapped[1 * 2 + 0] = (uint32_t)vaddr;
				next_mapped[1 * 2 + 1] = length;
				/* Update the recently used fields. */
				kern_addr_recent_block = (uint32_t *)block_mapped[1];
				kern_addr_recent_slot = 1;
				/* Free up the mappings we used. */
				i686_paging_stack_popa();
				i686_paging_stack_popa();
				return;
			}
		}
		/* If it wasn't free, we still need to find a new slot. */
	}
	else {
		/* It's in the same block, check normally. */
		if (block_mapped[(kern_addr_recent_slot + 1) * 2 + 1] == 0) {
			/* Yup, found an empty one, we can overwrite this. */
			block_mapped[(kern_addr_recent_slot + 1) * 2 + 0] = (uint32_t)vaddr;
			block_mapped[(kern_addr_recent_slot + 1) * 2 + 1] = (uint32_t)length;
			/* Free up the mappings we used. */
			i686_paging_stack_popa();
			return;
		}
	}
	/* Ok, that didn't work, we need to search for one. Start by searching the 
	recent block. */
	for (int slot = 1; slot < 1024; slot++) {
		if (block_mapped[slot * 2 + 1] == 0) {
			/* Yup, found an empty one, we can overwrite this. */
			block_mapped[slot * 2 + 0] = (uint32_t)vaddr;
			block_mapped[slot * 2 + 1] = (uint32_t)length;
			/* Update the recent entries. */
			kern_addr_recent_slot = slot;
			/* Free up the mappings we used. */
			i686_paging_stack_popa();
			return; 
		}
	}
	/* Event THAT didn't work, so we need to search all blocks. */
	/* But first, let's unmap the blocks we're not using. */
	i686_paging_stack_popa();
	uint32_t *current_block = kern_addr_first_block;
	uint32_t *current_mapped;
	do {
		if (current_block != kern_addr_recent_block) {
			/* Check so that we don't double-search the most recent block. */
			/* Map in the block we're searching. */
			current_mapped = i686_paging_stack_pusha(current_block);
			for (int slot = 1; slot < 1024; slot++) {
				if (current_mapped[slot * 2 + 1] == 0) {
					/* Yup, found an empty one, we can overwrite this. */
					current_mapped[slot * 2 + 0] = (uint32_t)vaddr;
					current_mapped[slot * 2 + 1] = (uint32_t)length;
					/* Update the recent entries. */
					kern_addr_recent_block = current_block;
					kern_addr_recent_slot = slot;
					/* Free up the mappings we used. */
					i686_paging_stack_popa();
					return;
				}
			}
		}
		/* Move on to the next block. We use the 2nd field of this one to get the next one. */
		current_block = (uint32_t *)(current_mapped[1]);
		i686_paging_stack_popa();
	} while (current_block != 0);

	/* If we STILL didn't find anything, we need to make a new block, and tack it
	on the end of the last one. */
	/* Find the end of the block chain. */
	current_block = kern_addr_recent_block;
	current_mapped = i686_paging_stack_pusha(current_block);
	while (current_mapped[1] != 0) {
		current_block = (uint32_t *)(current_mapped[1]);
		i686_paging_stack_popa();
		current_mapped = i686_paging_stack_pusha(current_block);
	}
	/* Make a new block. */
	block_mapped[1] = plist_get_page();
	uint32_t *newblock_mapped = i686_paging_stack_pusha((uint32_t *)(block_mapped[1]));
	mem_zero4(newblock_mapped, 4096 / 4);
	/* Make this point to the previous block. */
	newblock_mapped[0] = (uint32_t)current_block;
	/* Finally, fill in the first entry with the thing we wanted to use. */
	newblock_mapped[1 * 2 + 0] = (uint32_t)vaddr;
	newblock_mapped[1 * 2 + 1] = (uint32_t)length;
	kern_addr_recent_block = current_block;
	kern_addr_recent_slot = 1;
	/* Free up the blocks we were using. */
	i686_paging_stack_popa();
	i686_paging_stack_popa();

}
