# i686_mem.s
# Daniel Selmes 2016
# Assembly routines for fast memory manipulation. 

.section text
.global	mem_zero4
.type mem_zero4 STT_FUNC
mem_zero4:
	pushl %ebp				# Stack Frame
	movl %esp, %ebp
	pushl %edi				# Preserve EDI
	movl 8(%ebp), %edi		# Load arguments into the correct registers
	movl 12(%ebp), %ecx
	movl $0, %eax
	shrl $2, %ecx		# Correct the size of the area to zero (since each mov does 4 bytes)
	rep stosl				# Store AX (0) into [EDI] ECX times, or zeroes out 4*ECX bytes at EDI
	popl %edi				# Restore EDI
	popl %ebp				# Restore Stack frame
	ret

.global mem_zero2
.type mem_zero2 STT_FUNC
mem_zero2:
	pushl %ebp				# Stack Frame
	movl %esp, %ebp
	pushl %edi				# Preserve EDI
	movl 8(%ebp), %edi		# Load arguments into the correct registers
	movl 12(%ebp), %ecx
	movl $0, %eax
	shrl $1, %ecx			# Correct the size of the area to zero (since each mov does 2 bytes)
	rep stosw				# Store AX (0) into [EDI] ECX times, or zeroes out 4*ECX bytes at EDI
	popl %edi				# Restore EDI
	popl %ebp				# Restore Stack frame
	ret
