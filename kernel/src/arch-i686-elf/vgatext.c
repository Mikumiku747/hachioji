/* 
 * vgatext.c
 * Daniel Selmes 2016
 * Provides an early life terminal for the kernel using the VGA text mode. 
*/

#include <vgatext.h>
#include <stdint.h>
#include <i686_kernel_misc.h>

/* Initialises the VGA terminal and prepares all the values. */
void vgatext_init()
{
	/* First, set the buffer up and all that. */
	vga_cursor_x = 0;
	vga_cursor_y = 0;
	vgatext_color = VGAFCOL_GREEN | VGABCOL_BLACK;
	vgatext_buffer = (uint16_t *)VADDR(VGA_TEXTBUFFER);
}

void vgatext_set_color(uint8_t color) 
{
	vgatext_color = ((uint16_t)color) << 8;
}

void vgatext_scroll_console() 
{
	/* We loop over copying the nth row to the n-1 row. */
	for (int row = 1; row < VGA_HEIGHT; row++) {
		for (int entry = 0; entry < VGA_WIDTH; entry++) {
			vgatext_buffer[(VGA_WIDTH * (row - 1)) + entry] = vgatext_buffer[(VGA_WIDTH * row) + entry];
		}
	}
	for (int entry = 0; entry < VGA_WIDTH; entry++) {
		vgatext_buffer[(VGA_WIDTH * (VGA_HEIGHT - 1)) + entry] = vgatext_color | (uint16_t)' ';
	}
}

/* Print characters (including newlines), */
void vgatext_putc(char c)
{
	switch (c) {
	case '\n':
		vga_cursor_x = 0;
		vga_cursor_y++;
		if (vga_cursor_y >= VGA_HEIGHT) {
			vga_cursor_y--;
			vgatext_scroll_console();
		}
		break;
	case '\t':
		vga_cursor_x += 5;
		vga_cursor_x |= 0xFFFFFFFC;
		if (vga_cursor_x >= VGA_WIDTH) {
			vga_cursor_x = 0;
			vga_cursor_y++;
			if (vga_cursor_y >= VGA_HEIGHT) {
				vga_cursor_y--;
				vgatext_scroll_console();
			}
		}
		break;
	default:
		vgatext_buffer[vga_cursor_y * VGA_WIDTH + vga_cursor_x] = vgatext_color |(uint16_t)c;
		vga_cursor_x++;
		if (vga_cursor_x >= VGA_WIDTH) {
			vga_cursor_x = 0;
			vga_cursor_y++;
			if (vga_cursor_y >= VGA_HEIGHT) {
				vga_cursor_y--;
				vgatext_scroll_console();
			}
		}
		break;
	}
}
/* Print strings (including newlines). */
void vgatext_puts(char *string) 
{
	for (int i = 0; string[i] != '\0'; i++) {
		vgatext_putc(string[i]);
	}
}

/* Prints a 32 bit unsigned int in base 16. */
void vgatext_put_hexdw(uint32_t num)
{
	for (int i = 7; i >= 0; i--) {
		uint8_t nibble = (num >> i*4) & 0x0F;
		if (nibble < 0x0A) {
			vgatext_putc('0' + nibble);
		} else {
			vgatext_putc('A' + nibble - 0x0A);
		}
	}
}

/* Prints an 8 bit int in base 16. */
void vgatext_put_hexb(uint8_t num) 
{
	for (int i = 1; i >= 0; i--) {
		uint8_t nibble = (num >> i*4) & 0x0F;
		if (nibble < 0x0A) {
			vgatext_putc('0' + nibble);
		} else {
			vgatext_putc('A' + nibble - 0x0A);
		}
	}
}