/* 
 * pmm.c
 * Daniel Selmes 2016
 * Physical memory manager, i386 implementation.
*/

#include <pmm.h>
#include <stdint.h>
#include <plist_page_manager.h>
#include <platform_data.h>
#include <kerror_i686.h>

unsigned pmm_init(void *platform_data) {
	/* Let's dump the memmap to make sure we got things right. */
	plist_dump_mboot_mmap((struct platform_data *)platform_data);
	/* New we'll try to initialise the memory system. */
	unsigned temp = plist_init((struct platform_data *)platform_data);
	/* Dump the chain so we know the initalisation was successful. */
	plist_dump_stack_chain();
	return temp;
}

uintptr_t *pmm_get_page()
{
	uintptr_t *page = (uintptr_t *)plist_get_page();
	if (page != 0) {
		return page;
	} else {
		kerror_fatal_hang(0x01); /* Out of physical memory. */
		return 0;
	}
}

void pmm_free_page(uintptr_t *pageaddr) {
	plist_free_page((uint32_t *)pageaddr);
}
