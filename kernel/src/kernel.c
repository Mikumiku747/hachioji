/*
 * kernel.c
 * Daniel Selmes 2016
 * Platform independent kernel code, runs the policy side of the kernel. 
*/

#include <stdint.h>
#include <kterm.h>
#include <platform_data.h>
#include <pmm.h>
#include <mapper.h>
#include <i686_multiboot_module.h>

void kernel_main(struct platform_data *platform_data) {
	/* First thing to do is initialise early platform text output. */
	kterm_init();
	/* Now we can let people know the kernel is starting... */
	kterm_puts("Hachioji kernel is starting.\nHooray!\n");
	
	/* Start up the virtual memory manager. */
	mapping_init_temp(platform_data);

	/* Let's start up our physical memory manager. */
	unsigned maxmem = pmm_init((void *)platform_data);
	kterm_puts("Free memory: ");
	kterm_puti_uh(maxmem * 4);
	kterm_puts("K.\n");
	
	/* Test out the physical and virtual allocators. */
	uintptr_t *temp_pages[5];
	for (int i = 0; i < 5; i++) {
		temp_pages[i] = pmm_get_page();
		uintptr_t *access = mapping_temp_push(temp_pages[i]);
		for (int j = 0; j < 1024; j++) {
			access[j] = i;
		}
		kterm_puts("Write ");
		kterm_puti_uh(i);
		kterm_puts(" to ");
		kterm_puti_uh((unsigned)temp_pages[i]);
		kterm_puts(". ");
		mapping_temp_pop();
	}
	kterm_puts("\n");
	
	/* TEMPORARY: Dump some boot module info. */
	mboot_module_dump(platform_data);

	/* We shouldn't ever leave here, so let's just hang to be safe. */
	kterm_puts("DONE.");
	while(1);
}