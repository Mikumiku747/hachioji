# x86 Startup Process

Daniel Selmes 2016

## Loading the kernel

When the computer starts, whatever bootloader is installed is loaded. The 
bootloader, which is multiboot compliant, will load and start the kernel 
according to the multiboot specification. At this point, control is passed to
the bootstrap system.

## Bootstrapping
Once control is passed to the kernel, it does the minimum required to get x86
paging going. Just regular 2-level paging, not PAE or something else. Next, it
passes control to the pre-kernel bootstrapping:

## Pre-kernel
The pre-kernel stage takes the information from the boot process and abstracts 
it away from the rest of the kernel, so that it can be passed around in the 
platform independent parts of the kernel to the platform-dependent parts. 

## Kernel Init (Working on it)
At this point, we head into the meat of the kernel, and it begins some basic
system initialisation, including initialising the early-life terminal and the
memory management systems. Once the base of the kernel is done, it begins to 
load system level driver software. 

## Kernel Drivers (Unimplemented) 
Once the kernel has the most fundamental systems running, it will begin to load
drivers, which are configured in the bootloader. These run in kernel space, and
make use of some special kernel-only system calls to register interrupts and 
allocate memory kernel space memory and the like. 
