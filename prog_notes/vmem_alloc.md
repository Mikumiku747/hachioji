### Virtual Memory Allocator Notes 

Daniel Selmes 2016 

## Overall Structure
The overall structure of the virtual memory allocator is a list of free spaces.
Each free space is represented by a slot containing a start address and size, 
so during allocation, either a slot with the desired address range or a slot 
with the desired size can be found. 

## Block structure
The slots are stored in blocks, with 8 bytes at the start for some linking
structures followed by 1023 slot structures. The first 4 bytes of the block 
will point to the previous block in the chain, or 0 if it is the first. The 
next 4 bytes will point to the next block in the chain, or 0 if it is the last. 
When more slots are needed (ie. Fragmentation Occurs), The block chain can be 
expanded by creating a new end block and linking it to the old end block. 

## Slots
Each slot is 8 bytes, the first 4 being a pointer to the start of the free area 
and the next 4 being a the size of the free area. If the size is zero, the slot 
is not occupied, and can be used when adding a new free area to the list. Slots
are numbered from 1, so you can use them like so. 

    uint32_t *block;
    unsigned slot = 1;
    start = block[slot * 2]
	size = block[slot * 2 + 1]

## Fragmentation
This system gets fragmented pretty heavily, so there are a few ways to cut down 
on fragmentation. The first is to allocate out the end of a free area if 
possible, because this means that clumping back together fragmented free areas 
is slightly more efficient. Another step is to try to merge the free areas 
directly in front and behind an area when it is returned. 

## Allocating
1. Find a free area with enough spaces / the right address requirements for the
allocating party. Start by searching the last slot that was allocated from, and
if that fails, search the list in order. 
2. Reduce the length of the free area to the area you are taking, and return 
the area that you have just taken out of said free area.
3. Mark that area as the last area taken, so that future searches are faster.
4. Map in the page areas you just reserved. 

## Freeing
1. Find a free slot. If the slot after the one last allocated from is free, use
that, else, search linearly in this block and then in all blocks for a free 
slot. 
2. Fill that free slot with the area you are returning. 
3. Attempt to merge the slot you just made with the one behind it.  
